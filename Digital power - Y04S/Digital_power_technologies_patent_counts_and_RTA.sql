/*
Author:			Ashley ACKER (IEA)
Last updated:	October 2022
Description:	Creates a table (Digital_fractional_patent_counts) with fractional patent counts for digital power 
				technologies and a table (Digital_RTA) with the revealed technology advantage index. 
				All patent counts are by inventor country, year and technology.
				
Notes:			Tables referenced in script: Digital_tech, Digital_app_country_year, Digital_fractional_patent_counts, SPRING22_ENV_TECH,
					YO4S_mapping, SPRING22_APPLN, SPRING22_PERS_APPLN, SPRING22_PERSON

Important:		The user needs to create All_tech_fractional_patent_counts with script C:\Repos\iea_patstat\References\All_technologies_patent_counts
				as well as the table YO4S_mapping with script: C:\Repos\iea_patstat\Digital power - Y04S\TABLE_mapping_YO4S
				The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
					[GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo] -- this schema is where the original PATSTAT data is stored
*/
drop table if exists [Division_EDC].[patstats].Digital_tech
create table [Division_EDC].[patstats].Digital_tech( -- Note: This table was created in Patent_counts_digital_energy_technologies script
	appln_id int, 
	cpc_class_symbol varchar(30),
	Class_1 varchar(1000),
	Class_2 varchar(1000),
	Class_3 varchar(1000))  

drop table if exists [Division_EDC].[patstats].Digital_app_country_year
create table [Division_EDC].[patstats].Digital_app_country_year( -- Note: Intermediary table that was not created as a temp since it takes >1h to generate
	appln_id int, 
	Class_1 varchar(1000), -- YO4S categories
	Class_2 varchar(1000), -- YO4S categories
	Class_3 varchar(1000), -- YO4S categories
	CountryISO2 varchar(5),
	Patent_year int,
	person_id int )

drop table if exists [Division_EDC].[patstats].Digital_fractional_patent_counts
create table  [Division_EDC].[patstats].Digital_fractional_patent_counts(
	CountryISO2 varchar(5),
	Patent_year int,
	Hierarchy varchar(30),
	Category varchar(300),
	Fractional_patent_count  decimal(10,2))

drop table if exists [Division_EDC].[patstats].Digital_RTA
create table  [Division_EDC].[patstats].Digital_RTA(
	CountryISO2 varchar(5),
	Year_range varchar(30),
	Hierarchy varchar(30),
	Category varchar(300),
	RTA  decimal(10,4))

-- Filling the table of digital patent applications and the IEA category name we have assigned to them (currently this is the original YO4S description)
truncate table [Division_EDC].[patstats].Digital_tech
insert into [Division_EDC].[patstats].Digital_tech
select pat.appln_id, pat.cpc_class_symbol, map.Class_1, map.Class_2, map.Class_3
from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_APPLN_CPC] pat
left join [Division_EDC].[patstats].YO4S_mapping map
on pat.CPC_class_symbol = map.YO4S_Class
where pat.cpc_class_symbol in (select distinct YO4S_Class from [Division_EDC].[patstats].YO4S_mapping)

-- The Digital_app_country_year is an intermediary table that will be used to create the fractional patent counts. 
-- Runtime: 5 min 
-- It createst a table with inventor country of residence and priority year for digital power patents. 
-- Each row corresponds to an inventor of a given patent application.
truncate table [Division_EDC].[patstats].Digital_app_country_year 
insert into [Division_EDC].[patstats].Digital_app_country_year
select dig.appln_id, dig.Class_1, dig.Class_2, dig.Class_3, prio.Inv_ctry_code as CountryISO2, prio.Prio_year as Patent_year, prio.Person_id 
from [Division_EDC].[patstats].Digital_tech dig
-- Note: [SPRING22_PRIO_APP] only has priority application IDs, which allows us to count distinct inventions 
inner join (select * from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_PRIO_INV]) prio on prio.Priority_Appln_id = dig.Appln_id 
left join (select * from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_APPLN]) app on app.appln_id = dig.Appln_id
-- We only want the resulting table to include patents that have family size >=2 for which the country is known 
where docdb_family_size>=2 and Inv_ctry_code is not NULL and Inv_ctry_code != '' and Prio_year!='9999'

--------------------------------------------------------------------------------
-- Patent count prep for digital power technologies -- YO4S Class_1
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
-- 21 744 rows
-- Note: CountryISO3 is counted instead of distinct person_id because there are cases where the country of residence is known but not the person ID in the data
drop table if EXISTS #table_class1
select Patent_year, Class_1 as Category, appln_id , CountryISO2,  count(CountryISO2)  as Country_people
into #table_class1
from [Division_EDC].[patstats].Digital_app_country_year
group by appln_id,  Patent_year, Class_1 , CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when Digital_fractional_patent_counts is filled
drop table if EXISTS #fractional_count_prep_class1
select tab.CountryISO2 , tab.Patent_year , tab.Category, 'Class_1' as Hierarchy, tab.appln_id,    
	CAST ( tab.Country_people as decimal(38,6)) / tab_country_count.Total_country_people   as Fractional_count_prep
into #fractional_count_prep_class1
from (select * from #table_class1) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people) as Total_country_people
	from #table_class1
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------
-- Patent count prep for digital power technologies -- YO4S Class_2
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
-- 24 244 rows
drop table if EXISTS #table_class2
select Patent_year, Class_2 as Category, appln_id , CountryISO2,  count(CountryISO2) as Country_people
into #table_class2
from [Division_EDC].[patstats].Digital_app_country_year
group by appln_id,  Patent_year, Class_2 , CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when Digital_fractional_patent_counts is filled
drop table if EXISTS #fractional_count_prep_class2
select tab.CountryISO2 , tab.Patent_year , tab.Category, 'Class_2' as Hierarchy, tab.appln_id,    
	CAST ( tab.Country_people as decimal(38,6)) / tab_country_count.Total_country_people   as Fractional_count_prep
into #fractional_count_prep_class2
from (select * from #table_class2) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people) as Total_country_people
	from #table_class2
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category


--------------------------------------------------------------------------------
-- Patent count prep for digital power technologies -- YO4S Class_3
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
-- 23 736 rows
drop table if EXISTS #table_class3
select Patent_year, Class_3 as Category, appln_id , CountryISO2, count(CountryISO2) as Country_people
into #table_class3
from [Division_EDC].[patstats].Digital_app_country_year
group by appln_id,  Patent_year, Class_3 , CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when Digital_fractional_patent_counts is filled
drop table if EXISTS #fractional_count_prep_class3
select tab.CountryISO2 , tab.Patent_year , tab.Category, 'Class_3' as Hierarchy, tab.appln_id,    
	CAST ( tab.Country_people as decimal(38,6)) / tab_country_count.Total_country_people   as Fractional_count_prep
into #fractional_count_prep_class3
from (select * from #table_class3) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people)  as Total_country_people
	from #table_class3
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------
-- Patent count prep for total of digital power technologies
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
-- 16 580 rows
drop table if EXISTS #table_total
select Patent_year, 'Total - digital power technologies' as Category, appln_id , CountryISO2,  count(CountryISO2) as Country_people
into #table_total
from [Division_EDC].[patstats].Digital_app_country_year
group by appln_id,  Patent_year,  CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when Digital_fractional_patent_counts is filled
drop table if EXISTS #fractional_count_prep_total 
select tab.CountryISO2 , tab.Patent_year , tab.Category, 'Total' as Hierarchy, tab.appln_id,    
	CAST ( tab.Country_people as decimal(38,6)) / tab_country_count.Total_country_people   as Fractional_count_prep
into #fractional_count_prep_total
from (select * from #table_total) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people) as Total_country_people
	from #table_total
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-- Generating final fractional patent counts by country, year and technology for digital power technologies (17 019 rows)
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].Digital_fractional_patent_counts 
insert into [Division_EDC].[patstats].Digital_fractional_patent_counts 
select CountryISO2, Patent_year, Hierarchy, Category , sum(Fractional_count_prep) as Fractional_patent_count
	from ( 
	select * from  #fractional_count_prep_class1 union 
	select * from  #fractional_count_prep_class2 union 
	select * from  #fractional_count_prep_class3 union 
	select * from  #fractional_count_prep_total) tab 
	group by CountryISO2, Patent_year, Hierarchy, Category 
union 
select * from [Division_EDC].[patstats].All_tech_fractional_patent_counts total

-----------------------------------------------------------------------------------------------------------------------
-------------------------------------------- RTA (4 549 rows) ---------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
-- Generating revealed technology advantage (RTA) index by country, year and technology for digital power technologies (17 989 rows)
--
-- RTA = [(Inventor patent counts for technology i in country j)/(Total inventor patent counts in country j)]/
--       [(Inventor patent counts for technology i in world)/(Total inventor patent counts in world)] 
--
-----------------------------------------------------------------------------------------------------------------------

-- Creating temp version of Digital_fractional_patent_counts that has column with year ranges for 5 year intervals
drop table if EXISTS #Digital_fractional_patent_counts_year_ranges_prep 
select CountryISO2, Patent_year, case when Patent_year<2016 then cast((floor((Patent_year - 1)/5)*5+1) as varchar(10)) + '-' + cast((floor((Patent_year - 1)/5)*5+5) as varchar(10)) 
									  when Patent_year>=2016 then '2016-2021' end as Year_range, 
									  Hierarchy, Category, Fractional_patent_count
into #Digital_fractional_patent_counts_year_ranges_prep
from [Division_EDC].[patstats].Digital_fractional_patent_counts

-- Aggregated fractional counts by the new year range column 
drop table if EXISTS #Digital_fractional_patent_counts_year_ranges 
select CountryISO2, Year_range, Hierarchy, Category, sum(Fractional_patent_count) as Fractional_patent_count
into #Digital_fractional_patent_counts_year_ranges
from #Digital_fractional_patent_counts_year_ranges_prep
group by CountryISO2, Year_range, Hierarchy, Category

-- Generating RTA
truncate table [Division_EDC].[patstats].Digital_RTA 
insert into [Division_EDC].[patstats].Digital_RTA 
select tab.CountryISO2, tab.Year_range, tab.Hierarchy, tab.Category , 
	case when tab2.pat_country = 0 then 0 
		 when ( tab3.pat_tech_world / tab4.pat_world) = 0 then 0 
		 else ( (tab.Fractional_patent_count / tab2.pat_country ) / ( tab3.pat_tech_world / tab4.pat_world) ) end as RTA
from 
(select * from #Digital_fractional_patent_counts_year_ranges where Hierarchy != 'ALL TECH') tab -- Removing the fractional counts of all technologies, which would be nonsensical for RTA

left join 
(select CountryISO2, Year_range, sum(Fractional_patent_count) as pat_country
from #Digital_fractional_patent_counts_year_ranges
where Hierarchy = 'ALL TECH' -- Only selecting ALL TECH to get the total patent counts for all technologies in the given country
group by CountryISO2, Year_range ) tab2
on tab.CountryISO2 = tab2.CountryISO2 and tab.Year_range = tab2.Year_range

left join 
(select Hierarchy, Category, Year_range, sum(Fractional_patent_count) as pat_tech_world
from #Digital_fractional_patent_counts_year_ranges
where Hierarchy != 'ALL TECH' -- Removing the fractional counts of all technologies since the focus here is only on the digital power technologies
group by Hierarchy, Category, Year_range ) tab3
on tab.Hierarchy = tab3.Hierarchy and tab.Category = tab3.Category and tab.Year_range = tab3.Year_range

left join 
(select Year_range, sum(Fractional_patent_count) as pat_world
from #Digital_fractional_patent_counts_year_ranges
where Hierarchy = 'ALL TECH' -- Only selecting ALL TECH to get the total patent counts for all technologies in the world
group by Year_range ) tab4
on tab.Year_range = tab4.Year_range
