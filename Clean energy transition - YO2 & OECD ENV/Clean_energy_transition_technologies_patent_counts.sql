/*
Author:			Ashley ACKER (IEA)
Last updated:	October 2022
Description:	Creates a table (ENV_fractional_patent_counts) with fractional patent counts for clean energy 
				transition technologies. All patent counts are by country, year and technology.

Notes:			Tables referenced in script: ENV_app_country_year, ENV_fractional_patent_counts, SPRING22_ENV_TECH,
					ENV_to_IEA_mapping, SPRING22_APPLN, SPRING22_PRIO_INV

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
					[GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo] -- this schema is where the original PATSTAT data is stored

				The following scripts need to be run to create the referenced tables: 
					Clean energy transition - YO2 & OECD ENV\TABLE_mapping_OECD-ENV_to_clean_energy_transition_technologies
*/
drop table if exists [Division_EDC].[patstats].ENV_app_country_year
create table [Division_EDC].[patstats].ENV_app_country_year( -- Note: Intermediary table that was not created as a temp since it takes >1h to generate
	appln_id int, 
	Category varchar(300),
	CountryISO2 varchar(5),
	Patent_year int,
	person_id int  )

drop table if exists [Division_EDC].[patstats].ENV_fractional_patent_counts
create table  [Division_EDC].[patstats].ENV_fractional_patent_counts(
	CountryISO2 varchar(5),
	Patent_year int,
	Category varchar(300),
	Fractional_patent_count  decimal(10,2))
	
-- Creating temporary table of clean energy transition patents and the IEA category name we have assigned to them
drop table if EXISTS #env_tech_mapped
select env.Appln_id, map.DDS_patent_category 
into #env_tech_mapped
from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_ENV_TECH] env 
left join (select * from [Division_EDC].[patstats].ENV_to_IEA_mapping) map on map.ENV_TECH = env.Env_Tech
where env.Env_Tech in (select distinct ENV_TECH from [Division_EDC].[patstats].ENV_to_IEA_mapping where DDS_patent_category != '')

-- The ENV_app_country_year is an intermediary table that will be used to create the fractional patent counts. 
-- It adds inventor country and earliest publication year to the table of clean energy transition patents. 
-- Each row corresponds to an inventor of a given patent application.
truncate table [Division_EDC].[patstats].ENV_app_country_year 
insert into [Division_EDC].[patstats].ENV_app_country_year
select env.Appln_id as appln_id, env.DDS_patent_category as Category, prio.Inv_ctry_code as CountryISO2, prio.Prio_year as Patent_year, prio.Person_id 
from #env_tech_mapped env
-- Note: [SPRING22_PRIO_APP] only has priority application IDs, which allows us to count distinct inventions 
inner join (select * from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_PRIO_INV]) prio on prio.Priority_Appln_id = env.Appln_id 
left join (select * from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_APPLN]) app on app.appln_id = env.Appln_id
-- We only want the resulting table to include patents that have family size >=2 for which the country is known 
where docdb_family_size>=2 and Inv_ctry_code is not NULL and Inv_ctry_code != '' and Prio_year!='9999'

--------------------------------------------------------------------------------
-- Patent count prep for clean energy transition technologies 
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
drop table if EXISTS #table_category
select Patent_year, Category, appln_id , CountryISO2,  cast(count(CountryISO2 ) as decimal(38,2)) as Country_people
into #table_category
from [Division_EDC].[patstats].ENV_app_country_year
group by appln_id,  Patent_year, Category , CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when ENV_fractional_patent_counts is filled
drop table if EXISTS #fractional_count_prep_category 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	tab.Country_people / tab_country_count.Total_country_people  as Fractional_count_prep
into #fractional_count_prep_category
from (select * from #table_category) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people) as Total_country_people
	from #table_category
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------
-- Patent count prep for total of clean energy transition technologies
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
drop table if EXISTS #table_total
select Patent_year, 'Total - clean energy transition technologies' as Category, appln_id , CountryISO2,  cast(count(CountryISO2) as decimal(38,2)) as Country_people
into #table_total
from [Division_EDC].[patstats].ENV_app_country_year
group by appln_id,  Patent_year,  CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when ENV_fractional_patent_counts is filled
drop table if EXISTS #fractional_count_prep_total 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	tab.Country_people / tab_country_count.Total_country_people  as Fractional_count_prep
into #fractional_count_prep_total
from (select * from #table_total) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people) as Total_country_people
	from #table_total
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

-----------------------------------------------------------------------------------------------------------------------------------
-- Generating final fractional patent counts by country, year and technology for clean energy transition technologies (20 617 rows)
-----------------------------------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].ENV_fractional_patent_counts 
insert into [Division_EDC].[patstats].ENV_fractional_patent_counts 
select CountryISO2, Patent_year, Category , sum(Fractional_count_prep) as Fractional_patent_count
from ( select * from  #fractional_count_prep_category
union select * from  #fractional_count_prep_total) tab 
group by CountryISO2, Patent_year, Category 
