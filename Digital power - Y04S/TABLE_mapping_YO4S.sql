/*
Author:			Ashley ACKER (IEA)
Date:			September 2022
Description:	Creates a table (YO4S_mapping) mapping the YO4S patent categories to the IEA 
				digital energy categories. This table will be used to categorise patent applicaitons 
				in Patstat. 

Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: YO4S_mapping  

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
*/

create table [Division_EDC].[patstats].YO4S_mapping(
	YO4S_Class	varchar(300),
	Class_1	varchar(300),
	Class_2	varchar(300),
	Class_3	varchar(300)
	) 

truncate table  [Division_EDC].[patstats].YO4S_mapping
insert into [Division_EDC].[patstats].YO4S_mapping (YO4S_Class, Class_1, Class_2, Class_3)
select YO4S_Class, Class_1, Class_2, Class_3
 FROM 
(
select 'Y04S  10/00' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1, '' Class_2, '' Class_3  union 
select 'Y04S  20/12' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1, 'Energy storage units, uninterruptible power supply [UPS] systems or standby or emergency generators, e.g. in the last power distribution stages' Class_2, '' Class_3  union
select 'Y04S  20/14' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'Protecting elements, switches, relays or circuit breakers' Class_2, '' Class_3  union
select 'Y04S  20/20' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, '' Class_3  union
select 'Y04S  20/221' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, 'General power management systems' Class_3  union
select 'Y04S  20/222' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, 'Demand response systems, e.g. load shedding, peak shaving' Class_3  union
select 'Y04S  20/242' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, 'Home appliances' Class_3  union
select 'Y04S  20/244' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, 'Home appliances' Class_3  union
select 'Y04S  20/246' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, 'Home appliances' Class_3  union
select 'Y04S  20/248' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'End-user application control systems' Class_2, 'UPS systems or standby or emergency generators' Class_3  union
select 'Y04S  20/30' YO4S_Class, 'Management or operation of end-user stationary applications or the last stages of power distribution; Controlling, monitoring or operating thereof' Class_1 , 'Smart metering, e.g. specially adapted for remote reading' Class_2, '' Class_3  union
select 'Y04S  10/12' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Monitoring or controlling equipment for energy generation units, e.g. distributed energy generation [DER] or load-side generation' Class_2, '' Class_3  union
select 'Y04S  10/123' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Monitoring or controlling equipment for energy generation units, e.g. distributed energy generation [DER] or load-side generation' Class_2, 'The energy generation units being or involving renewable energy sources' Class_3  union
select 'Y04S  10/126' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Monitoring or controlling equipment for energy generation units, e.g. distributed energy generation [DER] or load-side generation' Class_2, 'The energy generation units being or involving electric vehicles [EV] or hybrid vehicles [HEV], i.e. power aggregation of EV or HEV, vehicle to grid arrangements [V2G]' Class_3  union
select 'Y04S  10/14' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Energy storage units' Class_2, '' Class_3  union
select 'Y04S  10/16' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Electric power substations' Class_2, '' Class_3  union
select 'Y04S  10/18' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Using switches, relays or circuit breakers, e.g. intelligent electronic devices [IED]' Class_2, '' Class_3  union
select 'Y04S  10/20' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Using protection elements, arrangements or systems' Class_2, '' Class_3  union
select 'Y04S  10/22' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Flexible AC transmission systems [FACTS] or power factor or reactive power compensating or correcting units' Class_2, '' Class_3  union
select 'Y04S  10/30' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'State monitoring, e.g. fault, temperature monitoring, insulator monitoring, corona discharge' Class_2, '' Class_3  union
select 'Y04S  10/40' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Display of information, e.g. of data or controls' Class_2, '' Class_3  union
select 'Y04S  10/50' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Systems or methods supporting the power network operation or management, involving a certain degree of interaction with the load-side end user applications' Class_2, '' Class_3  union
select 'Y04S  10/52' YO4S_Class, 'Systems supporting electrical power generation, transmission or distribution' Class_1 , 'Systems or methods supporting the power network operation or management, involving a certain degree of interaction with the load-side end user applications' Class_2, 'Outage or fault management, e.g. fault detection or location' Class_3  union
select 'Y04S  30/00' YO4S_Class, 'Systems supporting specific end-user applications in the sector of transportation' Class_1 , '' Class_2, '' Class_3  union
select 'Y04S  30/10' YO4S_Class, 'Systems supporting specific end-user applications in the sector of transportation' Class_1 , 'Systems supporting the interoperability of electric or hybrid vehicles' Class_2, '' Class_3  union
select 'Y04S  30/12' YO4S_Class, 'Systems supporting specific end-user applications in the sector of transportation' Class_1 , 'Systems supporting the interoperability of electric or hybrid vehicles' Class_2, 'Remote or cooperative charging' Class_3  union
select 'Y04S  30/14' YO4S_Class, 'Systems supporting specific end-user applications in the sector of transportation' Class_1 , 'Systems supporting the interoperability of electric or hybrid vehicles' Class_2, 'Details associated with the interoperability, e.g. vehicle recognition, authentication, identification or billing' Class_3  union
select 'Y04S  40/00' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , '' Class_2, '' Class_3  union
select 'Y04S  40/12' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Characterised by data transport means between the monitoring, controlling or managing units and monitored, controlled or operated electrical equipment' Class_2, '' Class_3  union
select 'Y04S  40/121' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Characterised by data transport means between the monitoring, controlling or managing units and monitored, controlled or operated electrical equipment' Class_2, 'Using the power network as support for the transmission' Class_3  union
select 'Y04S  40/124' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Characterised by data transport means between the monitoring, controlling or managing units and monitored, controlled or operated electrical equipment' Class_2, 'Using wired telecommunication networks or data transmission busses' Class_3  union
select 'Y04S  40/126' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Characterised by data transport means between the monitoring, controlling or managing units and monitored, controlled or operated electrical equipment' Class_2, 'Using wireless data transmission' Class_3  union
select 'Y04S  40/128' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Characterised by data transport means between the monitoring, controlling or managing units and monitored, controlled or operated electrical equipment' Class_2, 'Involving the use of Internet protocol' Class_3  union
select 'Y04S  40/18' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Network protocols supporting networked applications, e.g. including control of end-device applications over a network' Class_2, '' Class_3  union
select 'Y04S  40/20' YO4S_Class, 'Systems for electrical power generation, transmission, distribution or end-user application management characterised by the use of communication or information technologies, or communication or information technology specific aspects supporting them' Class_1 , 'Information technology specific aspects, e.g. CAD, simulation, modelling, system security' Class_2, '' Class_3  union
select 'Y04S  50/00' YO4S_Class, 'Market activities related to the operation of systems integrating technologies related to power network operation or related to communication or information technologies' Class_1 , '' Class_2, '' Class_3  union
select 'Y04S  50/10' YO4S_Class, 'Market activities related to the operation of systems integrating technologies related to power network operation or related to communication or information technologies' Class_1 , 'Energy trading, including energy flowing from end-user application to grid' Class_2, '' Class_3  union
select 'Y04S  50/12' YO4S_Class, 'Market activities related to the operation of systems integrating technologies related to power network operation or related to communication or information technologies' Class_1 , 'Billing, invoicing, buying or selling transactions or other related activities, e.g. cost or usage evaluation' Class_2, '' Class_3  union
select 'Y04S  50/14' YO4S_Class, 'Market activities related to the operation of systems integrating technologies related to power network operation or related to communication or information technologies' Class_1 , 'Marketing, i.e. market research and analysis, surveying, promotions, advertising, buyer profiling, customer management or rewards' Class_2, '' Class_3  union
select 'Y04S  50/16' YO4S_Class, 'Market activities related to the operation of systems integrating technologies related to power network operation or related to communication or information technologies' Class_1, 'Energy services, e.g. dispersed generation or demand or load or energy savings aggregation' Class_2, '' Class_3  
)a

