/*
Author:			Ashley ACKER (IEA)
Date:			September 2022
Description:	Creates a table (Digital__authority_fractional_patent_counts) with fractional patent counts for digital energy 
				technologies. All patent counts are by application authority, year and technology.
				The YO4S_mapping and Authorities_ISO2_mapping tables are required to run the script. 

Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: Digital_tech, Digital_app_authority_year, Digital__authority_fractional_patent_counts, SPRING22_ENV_TECH,
					Authorities_ISO2_mapping, YO4S_mapping, SPRING22_APPLN, SPRING22_PERS_APPLN, SPRING22_PERSON

Important:		The table Digital_tech needs to be created from script Patent_counts_digital_energy_technologies.sql 
				The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
					[GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo] -- this schema is where the original PATSTAT data is stored
*/

create table [Division_EDC].[patstats].Digital_app_authority_year( -- Note: Intermediary table that was not created as a temp since it takes >1h to generate
	appln_id int, 
	Class_1 varchar(1000), -- YO4S categories
	Class_2 varchar(1000), -- YO4S categories
	Class_3 varchar(1000), -- YO4S categories
	Authority varchar(300),
	Patent_year int)
create table  [Division_EDC].[patstats].Digital__authority_patent_counts(
	Authority varchar(300),
	Patent_year int,
	Hierarchy varchar(30),
	Category varchar(300),
	Patent_count int)
	
-- The Digital_app_country_year is an intermediary table that will be used to create the fractional patent counts. 
-- Runtime 11 min, 116 386 rows
-- It adds application authority and earliest publication year to the table of digital-related patents. 
truncate table [Division_EDC].[patstats].Digital_app_authority_year 
insert into [Division_EDC].[patstats].Digital_app_authority_year
select dig.appln_id, dig.Class_1, dig.Class_2, dig.Class_3, map.Authority, app.earliest_filing_year as Patent_year
from [Division_EDC].[patstats].Digital_tech dig
left join (select * from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_APPLN]) app on app.appln_id = dig.Appln_id
left join (select * from [Division_EDC].[patstats].Authorities_ISO2_mapping) map on app.Appln_auth=map.ISO2_code
-- We only want the resulting table to include patents that have family size >=2  
where docdb_family_size>=2 and earliest_filing_year!='9999' and Appln_auth is not NULL and Appln_auth != '' 

--------------------------------------------------------------------------------
-- Patent counts for digital technologies - YO4S Class_1
--------------------------------------------------------------------------------
drop table if EXISTS #table_class1_fractional_counts
select Patent_year, Class_1 as Category, 'Class_1' as Hierarchy, Authority,  count(distinct appln_id ) as Patent_count
into #table_class1_fractional_counts
from [Division_EDC].[patstats].Digital_app_authority_year
group by Patent_year, Class_1 , Authority

--------------------------------------------------------------------------------
-- Patent counts for digital technologies - YO4S Class_2
--------------------------------------------------------------------------------
drop table if EXISTS #table_class2_fractional_counts
select Patent_year, Class_2 as Category, 'Class_2' as Hierarchy, Authority,  count(distinct appln_id ) as Patent_count
into #table_class2_fractional_counts
from [Division_EDC].[patstats].Digital_app_authority_year
group by Patent_year, Class_2 , Authority

--------------------------------------------------------------------------------
-- Patent counts for digital technologies - YO4S Class_3
--------------------------------------------------------------------------------
drop table if EXISTS #table_class3_fractional_counts
select Patent_year, Class_3 as Category, 'Class_3' as Hierarchy, Authority,  count(distinct appln_id ) as Patent_count
into #table_class3_fractional_counts
from [Division_EDC].[patstats].Digital_app_authority_year
group by Patent_year, Class_3 , Authority

--------------------------------------------------------------------------------
-- Patent counts for total of digital technologies
--------------------------------------------------------------------------------
drop table if EXISTS #table_total_fractional_counts
select Patent_year, 'Total - digital power technologies' as Category, 'Total' as Hierarchy, Authority, count(distinct appln_id ) as Patent_count
into #table_total_fractional_counts
from [Division_EDC].[patstats].Digital_app_authority_year
group by Patent_year, Authority

-----------------------------------------------------------------------------------------------------------------------
-- Generating final fractional patent counts by country, year and technology for digital technologies
-----------------------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].Digital__authority_patent_counts 
insert into [Division_EDC].[patstats].Digital__authority_patent_counts 
select Authority, Patent_year, Hierarchy, Category, Patent_count
from ( 
select * from  #table_class1_fractional_counts union 
select * from  #table_class2_fractional_counts union 
select * from  #table_class3_fractional_counts union 
select * from  #table_total_fractional_counts
) tab 
