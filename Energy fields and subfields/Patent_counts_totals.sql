/*
Author:			Ashley ACKER (IEA)
Date:			July 2022
Description:	Calculates the fractional patent counts for all energy technologies and creates a table 
				(FULL_fractional_patent_counts) that merges the energy total with the fraction patent 
				counts for fossil fuel technologies and for clean energy transition technologies. All patent 
				counts are by country, year and technology. 

Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: 
					FULL_fractional_patent_counts, ENV_app_country_year, FF_fractional_patent_counts

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
*/

create table [Division_EDC].[patstats].FULL_fractional_patent_counts(
	CountryISO2 varchar(5),
	Patent_year int,
	Category varchar(300),
	Fractional_patent_count  decimal(10,2))
	
-- Merging the preparation tables of clean energy transition and fossil fuel technologies
drop table if exists #merged_app_table
select * into #merged_app_table
from [Division_EDC].[patstats].ENV_app_country_year
union (select appln_id, FF_supply_category as Category, CountryISO2, Patent_year, person_id 
	   from [Division_EDC].[patstats].FF_app_country_year)

-- Creating preparation table to generate fractional patent counts for all energy technologies.
-- The final step to generate the fractional counts is done when the final table (FULL_fractional_patent_counts) 
-- is filled under 'FINAL TABLE'. 
drop table if exists #table_full_total
select Patent_year, 'Total - energy technologies' as Category, appln_id , CountryISO2,  cast(count(distinct person_id  ) as decimal(38,2)) as Country_people
into #table_full_total
from #merged_app_table
group by appln_id,  Patent_year,  CountryISO2

drop table if exists #fractional_count_prep_full_total 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	CAST ( tab.Country_people / tab_country_count.Total_country_people as decimal(38,2))  as Fractional_count_prep
into #fractional_count_prep_full_total
from (select * from #table_full_total) tab
	-- Table that count the number of people for all countries (per application ID, year, and tech). 
left join 
	(select Patent_year, Category, appln_id , cast(sum(Country_people) as decimal(38,2)) as Total_country_people
	from #table_full_total
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------------------------------------------
-------------------------------- FINAL TABLE----------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].FULL_fractional_patent_counts 
insert into [Division_EDC].[patstats].FULL_fractional_patent_counts
select * from [Division_EDC].[patstats].FF_fractional_patent_counts 
union (select * from [Division_EDC].[patstats].ENV_fractional_patent_counts)
-- Generating fractional counts for all energy technologies
union (select CountryISO2, Patent_year, Category , sum(Fractional_count_prep) as Fractional_patent_count
	from #fractional_count_prep_full_total
	group by CountryISO2, Patent_year, Category)



