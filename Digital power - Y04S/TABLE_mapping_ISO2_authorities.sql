/*
Author:			Ashley ACKER (IEA)
Date:			September 2022
Description:	Creates a table (Authorities_ISO2_mapping) mapping the YO4S patent categories to the IEA 
				digital energy categories. This table will be used to categorise patent applicaitons 
				in Patstat. 

Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: Authorities_ISO2_mapping  

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved

Source for additional ISO2 codes: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#EP
*/

create table [Division_EDC].[patstats].Authorities_ISO2_mapping(
	ISO2_code	varchar(10),
	Authority	varchar(300)) 

truncate table  [Division_EDC].[patstats].Authorities_ISO2_mapping
insert into [Division_EDC].[patstats].Authorities_ISO2_mapping (ISO2_code, Authority)
select [ISO2] as ISO2_code, [fullName] as Authority from [IEA_DW].[dw].[DimRegion] where [ISO2] is not null 
union
(select ISO2_code, Authority
 from (
select 'AP'	ISO2_code, 'African Regional Industrial Property Organization (ARIPO)' Authority union 
select 'BX'	ISO2_code, 'Benelux Trademarks and Designs Office (BOIP)' Authority union
select 'EF'	ISO2_code, 'Union of Countries under the European Community Patent Convention' Authority union
select 'EM'	ISO2_code, 'European Trademark Office (EUIPO)' Authority union
select 'EP'	ISO2_code, 'European Patent Organization' Authority union
select 'EV'	ISO2_code, 'Eurasian Patent Organization (EAPO)' Authority union
select 'GC'	ISO2_code, 'Patent Office of the Cooperation Council for the Arab States of the Gulf (GCCPO)' Authority union
select 'IB'	ISO2_code, 'International Bureau of WIPO' Authority union
select 'OA'	ISO2_code, 'African Intellectual Property Organization (OAPI)' Authority union
select 'WO'	ISO2_code, 'World Intellectual Property Organization (WIPO)' Authority 
) additions ) 