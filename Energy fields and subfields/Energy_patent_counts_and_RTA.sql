/*
Author:			Ashley ACKER (IEA)
Last updated:	October 2022
Description:	Calculates the fractional patent counts for all energy technologies and creates a table 
				(FULL_fractional_patent_counts) that merges the energy total with the fraction patent 
				counts for fossil fuel technologies and for clean energy transition technologies and all 
				technologies. All patent counts are by country, year and technology. 
				The script also creates a table Energy_RTA which gives the revealed technology advantage for 
				all energy technologies (total, fossil fuels, clean energy transition).

Notes:			Tables referenced in script: 
					FULL_fractional_patent_counts, ENV_app_country_year, FF_fractional_patent_counts, 
					All_tech_fractional_patent_counts, Energy_RTA

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved

				The following scripts need to be run to create the referenced tables: 
					Clean energy transition - YO2 & OECD ENV\Clean_energy_transition_technologies_patent_counts
					Fossil fuels - CPC\Fossil_fuel_technologies_patent_counts
					References\All_technologies_patent_counts
*/
drop table if exists [Division_EDC].[patstats].FULL_fractional_patent_counts
create table [Division_EDC].[patstats].FULL_fractional_patent_counts(
	CountryISO2 varchar(5),
	Patent_year int,
	Category varchar(300),
	Fractional_patent_count  decimal(10,2))

drop table if exists [Division_EDC].[patstats].Energy_RTA
create table  [Division_EDC].[patstats].Energy_RTA(
	CountryISO2 varchar(5),
	Year_range varchar(30),
	Category varchar(300),
	RTA  decimal(10,4))

-- Merging the preparation tables of clean energy transition and fossil fuel technologies
drop table if exists #merged_app_table
select * into #merged_app_table
from [Division_EDC].[patstats].ENV_app_country_year
union (select appln_id, FF_supply_category as Category, CountryISO2, Patent_year, person_id 
	   from [Division_EDC].[patstats].FF_app_country_year)

-- Creating preparation table to generate fractional patent counts for all energy technologies.
-- The final step to generate the fractional counts is done when the final table (FULL_fractional_patent_counts) 
-- is filled under 'FINAL PATENT COUNT TABLE'. 
drop table if exists #table_full_total
select Patent_year, 'Total - energy technologies' as Category, appln_id , CountryISO2,  cast(count(CountryISO2) as decimal(38,6)) as Country_people
into #table_full_total
from #merged_app_table
group by appln_id,  Patent_year,  CountryISO2

drop table if exists #fractional_count_prep_full_total 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	tab.Country_people / tab_country_count.Total_country_people as Fractional_count_prep
into #fractional_count_prep_full_total
from (select * from #table_full_total) tab
	-- Table that count the number of people for all countries (per application ID, year, and tech). 
left join 
	(select Patent_year, Category, appln_id , sum(Country_people) as Total_country_people
	from #table_full_total
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------------------------------------------
-------------------------------- FINAL PATENT COUNT TABLE (41 366 rows) --------------------------------------------
--------------------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].FULL_fractional_patent_counts 
insert into [Division_EDC].[patstats].FULL_fractional_patent_counts
select * from [Division_EDC].[patstats].FF_fractional_patent_counts 
union (select * from [Division_EDC].[patstats].ENV_fractional_patent_counts)
union (select CountryISO2, Patent_year, Category, Fractional_patent_count from [Division_EDC].[patstats].All_tech_fractional_patent_counts )
-- Generating fractional counts for all energy technologies
union (select CountryISO2, Patent_year, Category , sum(Fractional_count_prep) as Fractional_patent_count
	from #fractional_count_prep_full_total
	group by CountryISO2, Patent_year, Category)
	
-----------------------------------------------------------------------------------------------------------------------
-------------------------------------------- RTA table (12 069 rows) --------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
-- Generating revealed technology advantage (RTA) index by country, year and technology for energy technologies (17 989 rows)
--
-- RTA = [(Inventor patent counts for technology i in country j)/(Total inventor patent counts in country j)]/
--       [(Inventor patent counts for technology i in world)/(Total inventor patent counts in world)] 
--
-----------------------------------------------------------------------------------------------------------------------

-- Creating temp version of Digital_fractional_patent_counts that has column with year ranges for 5 year intervals
drop table if EXISTS #Energy_fractional_patent_counts_year_ranges_prep 
select CountryISO2, Patent_year, case when Patent_year<2016 then cast((floor((Patent_year - 1)/5)*5+1) as varchar(10)) + '-' + cast((floor((Patent_year - 1)/5)*5+5) as varchar(10)) 
									  when Patent_year>=2016 then '2016-2021' end as Year_range, 
									  Category, Fractional_patent_count
into #Energy_fractional_patent_counts_year_ranges_prep
from [Division_EDC].[patstats].FULL_fractional_patent_counts

-- Aggregated fractional counts by the new year range column 
drop table if EXISTS #Energy_fractional_patent_counts_year_ranges 
select CountryISO2, Year_range, Category, sum(Fractional_patent_count) as Fractional_patent_count
into #Energy_fractional_patent_counts_year_ranges
from #Energy_fractional_patent_counts_year_ranges_prep
group by CountryISO2, Year_range, Category

-- Generating RTA
truncate table [Division_EDC].[patstats].Energy_RTA 
insert into [Division_EDC].[patstats].Energy_RTA 
select tab.CountryISO2, tab.Year_range, tab.Category , 
	case when tab2.pat_country = 0 then 0 
		 when ( tab3.pat_tech_world / tab4.pat_world) = 0 then 0 
		 else ( (tab.Fractional_patent_count / tab2.pat_country ) / ( tab3.pat_tech_world / tab4.pat_world) ) end as RTA
from 
(select * from #Energy_fractional_patent_counts_year_ranges where Category != 'Total - all technologies') tab -- Removing the fractional counts of all technologies, which would be nonsensical for RTA

left join 
(select CountryISO2, Year_range, sum(Fractional_patent_count) as pat_country
from #Energy_fractional_patent_counts_year_ranges
where Category = 'Total - all technologies' -- filter to get the total patent counts for all technologies in the world without duplicates
group by CountryISO2, Year_range ) tab2
on tab.CountryISO2 = tab2.CountryISO2 and tab.Year_range = tab2.Year_range

left join 
(select Category, Year_range, sum(Fractional_patent_count) as pat_tech_world
from #Energy_fractional_patent_counts_year_ranges
where Category != 'Total - all technologies' -- Removing the fractional counts of all technologies since the focus here is only on the digital power technologies
group by Category, Year_range ) tab3
on tab.Category = tab3.Category and tab.Year_range = tab3.Year_range

left join 
(select Year_range, sum(Fractional_patent_count) as pat_world
from #Energy_fractional_patent_counts_year_ranges
where Category = 'Total - all technologies' -- filter to get the total patent counts for all technologies in the world without duplicates
group by Year_range ) tab4
on tab.Year_range = tab4.Year_range


