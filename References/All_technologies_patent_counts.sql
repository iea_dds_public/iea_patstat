/*
Author:			Ashley ACKER (IEA)
Last updated:	October 2022
Description:	Creates a table (All_tech_fractional_patent_counts) with fractional patent counts for all 
				technologies by country and year. 

Notes:			Tables referenced in script: All_tech_fractional_patent_counts, SPRING22_APPLN, SPRING22_PRIO_INV

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
					[GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo] -- this schema is where the original PATSTAT data is stored
*/
drop table if exists [Division_EDC].[patstats].All_tech_fractional_patent_counts
create table  [Division_EDC].[patstats].All_tech_fractional_patent_counts(
	CountryISO2 varchar(5),
	Patent_year int,
	Hierarchy varchar(30),
	Category varchar(300),
	Fractional_patent_count  decimal(10,4))

drop table if exists [Division_EDC].[patstats].All_tech_app_country_year
create table [Division_EDC].[patstats].All_tech_app_country_year( 
	appln_id int, 
	CountryISO2 varchar(5),
	Patent_year int,
	person_id int )

-- The All_tech_app_country_year is an intermediary table that will be used to create the fractional patent counts. 
-- It adds inventor country and earliest publication year to the table of patents applications. 
-- Each row corresponds to an inventor of a given patent application.
-- 16 998 365 rows affected 
truncate table [Division_EDC].[patstats].All_tech_app_country_year 
insert into [Division_EDC].[patstats].All_tech_app_country_year
select prio.Priority_Appln_id as appln_id, prio.Inv_ctry_code as CountryISO2, prio.Prio_year as Patent_year, prio.Person_id  
from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_PRIO_INV] prio
left join (select * from [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_APPLN]) app on prio.Priority_Appln_id = app.appln_id
-- We only want the resulting table to include patents that have family size >=2 for which the country is known 
where docdb_family_size>=2 and Inv_ctry_code is not NULL and Inv_ctry_code != '' and Prio_year!='9999'

--------------------------------------------------------------------------------
-- Patent count prep for total of all technologies
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID and year)
drop table if EXISTS #table_all_tech
select CountryISO2, Patent_year, appln_id,  cast(count(CountryISO2) as decimal(38,6)) as Country_people
into #table_all_tech
from [Division_EDC].[patstats].All_tech_app_country_year
group by CountryISO2, Patent_year, appln_id

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when All_tech_fractional_patent_counts is filled
drop table if EXISTS #table_all_tech_fractional_count_prep
select tab.CountryISO2 , tab.Patent_year ,  tab.appln_id,    
	tab.Country_people / tab_country_count.Total_country_people  as Fractional_count_prep
into #table_all_tech_fractional_count_prep
from (select * from #table_all_tech) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, appln_id , sum(Country_people) as Total_country_people
	from #table_all_tech
	group by appln_id,  Patent_year) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year 

-----------------------------------------------------------------------------------------------------------------------
-- Generating final fractional patent counts by country, year and technology for all technologies (9 329 rows)
-----------------------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].All_tech_fractional_patent_counts 
insert into [Division_EDC].[patstats].All_tech_fractional_patent_counts 
select CountryISO2, Patent_year, 'ALL TECH' as Hierarchy, 'Total - all technologies' as Category , sum(Fractional_count_prep) as Fractional_patent_count
from (select * from  #table_all_tech_fractional_count_prep) tab 
group by CountryISO2, Patent_year 