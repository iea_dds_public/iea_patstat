/*
Author:			Ashley ACKER (IEA)
Date:			July 2022
Description:	Creates a table (ENV_to_IEA_mapping) mapping the OECD/ENV patent categories to the IEA 
				clean energy transition categories. This table will be used to categorise patent applicaitons 
				in Patstat. 

Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: ENV_to_IEA_mapping  

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
*/

create table [Division_EDC].[patstats].ENV_to_IEA_mapping(
	ENV_TECH	varchar(300),
	HIERARCHY	varchar(300),
	EN	varchar(300),
	Domain_to_publish varchar(300),
	DDS_patent_category	varchar(300),
	To_keep varchar(300) ) 

truncate table  [Division_EDC].[patstats].ENV_to_IEA_mapping
insert into [Division_EDC].[patstats].ENV_to_IEA_mapping (ENV_TECH,HIERARCHY, EN,  Domain_to_publish, DDS_patent_category,  To_keep)
select ENV_TECH, HIERARCHY, EN,  Domain_to_publish, DDS_patent_category,  To_keep
 FROM
(
select 'ADAPT' ENV_TECH, 'TOTAL' HIERARCHY, 'Climate change adaptation technologies' EN, NULL Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_1' ENV_TECH, 'ADAPT' HIERARCHY, 'Adaptation at coastal zones or river basin' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_1_1' ENV_TECH, 'ADAPT_1' HIERARCHY, 'Hard structures, e.g. dams, dykes or breakwaters' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_1_2' ENV_TECH, 'ADAPT_1' HIERARCHY, 'Dune restoration or creation; cliff stabilisation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_1_3' ENV_TECH, 'ADAPT_1' HIERARCHY, 'Artificial reefs or seaweed; restoration or protection of coral reefs' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_1_4' ENV_TECH, 'ADAPT_1' HIERARCHY, 'Flood prevention; flood or storm water management' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_1_5' ENV_TECH, 'ADAPT_1' HIERARCHY, 'Controlling, monitoring or forecasting' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2' ENV_TECH, 'ADAPT' HIERARCHY, 'Water resource management' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_2_1' ENV_TECH, 'ADAPT_2' HIERARCHY, 'Demand-side technologies (water conservation)' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_2_1_1' ENV_TECH, 'ADAPT_2_1' HIERARCHY, 'Indoor water conservation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_1_2' ENV_TECH, 'ADAPT_2_1' HIERARCHY, 'Irrigation water conservation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_1_3' ENV_TECH, 'ADAPT_2_1' HIERARCHY, 'Water conservation in thermoelectric power production' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_2' ENV_TECH, 'ADAPT_2' HIERARCHY, 'Supply-side technologies (water availability)' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_2_2_1' ENV_TECH, 'ADAPT_2_2' HIERARCHY, 'Water desalination' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_2_2' ENV_TECH, 'ADAPT_2_2' HIERARCHY, 'Water filtration; Water and wastewater treatment' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_2_3' ENV_TECH, 'ADAPT_2_2' HIERARCHY, 'Water collection (rain, surface and ground-water)' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_2_4' ENV_TECH, 'ADAPT_2_2' HIERARCHY, 'Water storage and distribution' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_2_2_5' ENV_TECH, 'ADAPT_2_2' HIERARCHY, 'Protecting water resources' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_3' ENV_TECH, 'ADAPT' HIERARCHY, 'Adapting or protecting infrastructure or their operation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_3_1' ENV_TECH, 'ADAPT_3' HIERARCHY, 'Extreme weather resilient electric power supply systems' EN, 'ADAPT' Domain_to_publish, 'Grid' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_3_2' ENV_TECH, 'ADAPT_3' HIERARCHY, 'Structural elements or technology for improving thermal insulation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_3_3' ENV_TECH, 'ADAPT_3' HIERARCHY, 'Relating to heating, ventilation or air conditioning [HVAC] technologies' EN, 'ADAPT' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_3_4' ENV_TECH, 'ADAPT_3' HIERARCHY, 'In transportation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_3_5' ENV_TECH, 'ADAPT_3' HIERARCHY, 'Planning or developing urban green infrastructure' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_4' ENV_TECH, 'ADAPT' HIERARCHY, 'Adaptation technologies in agriculture, forestry, livestock or agro-alimentary production' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_4_1' ENV_TECH, 'ADAPT_4' HIERARCHY, 'In agriculture' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_4_2' ENV_TECH, 'ADAPT_4' HIERARCHY, 'Ecological corridors or buffer zones' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_4_3' ENV_TECH, 'ADAPT_4' HIERARCHY, 'In food processing or handling, e.g. food conservation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_4_4' ENV_TECH, 'ADAPT_4' HIERARCHY, 'In livestock or poultry' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_4_5' ENV_TECH, 'ADAPT_4' HIERARCHY, 'In fisheries management' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_5' ENV_TECH, 'ADAPT' HIERARCHY, 'Adaptation technologies in human health protection, e.g. against extreme weather' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_5_1' ENV_TECH, 'ADAPT_5' HIERARCHY, 'Air quality improvement or preservation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_5_2' ENV_TECH, 'ADAPT_5' HIERARCHY, 'Against vector-borne diseases whose impact is exacerbated by climate change' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_6' ENV_TECH, 'ADAPT' HIERARCHY, 'Technologies having an indirect contribution to adaptation to climate change' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ADAPT_6_1' ENV_TECH, 'ADAPT_6' HIERARCHY, 'Information and communication technologies [ICT] supporting adaptation to climate change, e.g. for weather forecasting or climate simulation' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_6_2' ENV_TECH, 'ADAPT_6' HIERARCHY, 'Assessment of water resources' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ADAPT_6_3' ENV_TECH, 'ADAPT_6' HIERARCHY, 'Monitoring or fighting invasive species' EN, 'ADAPT' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'BUILD' ENV_TECH, 'CCM' HIERARCHY, 'Climate change mitigation technologies related to buildings' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'BUILD_1' ENV_TECH, 'BUILD' HIERARCHY, 'Integration of renewable energy sources in buildings' EN, 'BUILD' Domain_to_publish, 'Renewable energy integration in buildings' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_2' ENV_TECH, 'BUILD' HIERARCHY, 'Energy efficiency in buildings' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'No' To_keep union
select 'BUILD_2_1' ENV_TECH, 'BUILD_2' HIERARCHY, 'Energy efficient lighting' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_2_2' ENV_TECH, 'BUILD_2' HIERARCHY, 'Energy efficient heating, ventilation or air conditioning [HVAC]' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_2_3' ENV_TECH, 'BUILD_2' HIERARCHY, 'Energy efficiency in home appliances' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_2_4' ENV_TECH, 'BUILD_2' HIERARCHY, 'Energy efficient elevators, escalators and moving walkways, e.g. energy saving or recuperation technologies' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_2_5' ENV_TECH, 'BUILD_2' HIERARCHY, 'End-user side' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_3' ENV_TECH, 'BUILD' HIERARCHY, 'Architectural or constructional elements improving the thermal performance of buildings' EN, 'BUILD' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'BUILD_4' ENV_TECH, 'BUILD' HIERARCHY, 'Enabling technologies in buildings' EN, 'BUILD' Domain_to_publish, 'Building energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'CCM' ENV_TECH, 'ENV_TECH' HIERARCHY, 'Climate change mitigation' EN, NULL Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ENE' ENV_TECH, 'CCM' HIERARCHY, 'Climate change mitigation technologies related to energy generation, transmission or distribution' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ENE_1' ENV_TECH, 'ENE' HIERARCHY, 'Renewable energy generation' EN, 'ENE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ENE_1_1' ENV_TECH, 'ENE_1' HIERARCHY, 'Wind energy' EN, 'ENE' Domain_to_publish, 'Wind' DDS_patent_category, 'Yes' To_keep union
select 'ENE_1_2' ENV_TECH, 'ENE_1' HIERARCHY, 'Solar thermal energy' EN, 'ENE' Domain_to_publish, 'Solar' DDS_patent_category, 'Yes' To_keep union
select 'ENE_1_3' ENV_TECH, 'ENE_1' HIERARCHY, 'Solar photovoltaic (PV) energy' EN, 'ENE' Domain_to_publish, 'Solar' DDS_patent_category, 'Yes' To_keep union
select 'ENE_1_4' ENV_TECH, 'ENE_1' HIERARCHY, 'Solar thermal-PV hybrids' EN, 'ENE' Domain_to_publish, 'Solar' DDS_patent_category, 'Yes' To_keep union
select 'ENE_1_5' ENV_TECH, 'ENE_1' HIERARCHY, 'Geothermal energy' EN, 'ENE' Domain_to_publish, 'Other renewables' DDS_patent_category, 'Yes' To_keep union
select 'ENE_1_6' ENV_TECH, 'ENE_1' HIERARCHY, 'Marine energy, e.g. using wave energy or salinity gradient' EN, 'ENE' Domain_to_publish, 'Other renewables' DDS_patent_category, 'Yes' To_keep union
select 'ENE_1_7' ENV_TECH, 'ENE_1' HIERARCHY, 'Hydro energy' EN, 'ENE' Domain_to_publish, 'Hydro' DDS_patent_category, 'Yes' To_keep union
select 'ENE_2' ENV_TECH, 'ENE' HIERARCHY, 'Energy generation from fuels of non-fossil origin' EN, 'ENE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ENE_2_1' ENV_TECH, 'ENE_2' HIERARCHY, 'Biofuels, e.g. bio-diesel' EN, 'ENE' Domain_to_publish, 'Bioenergy' DDS_patent_category, 'Yes' To_keep union
select 'ENE_2_2' ENV_TECH, 'ENE_2' HIERARCHY, 'Fuel from waste, e.g. synthetic alcohol or diesel' EN, 'ENE' Domain_to_publish, 'Bioenergy' DDS_patent_category, 'Yes' To_keep union
select 'ENE_3' ENV_TECH, 'ENE' HIERARCHY, 'Nuclear energy' EN, 'ENE' Domain_to_publish, 'Nuclear' DDS_patent_category, 'No' To_keep union
select 'ENE_3_1' ENV_TECH, 'ENE_3' HIERARCHY, 'Nuclear fusion reactors' EN, 'ENE' Domain_to_publish, 'Nuclear' DDS_patent_category, 'Yes' To_keep union
select 'ENE_3_2' ENV_TECH, 'ENE_3' HIERARCHY, 'Nuclear fission reactors' EN, 'ENE' Domain_to_publish, 'Nuclear' DDS_patent_category, 'Yes' To_keep union
select 'ENE_4' ENV_TECH, 'ENE' HIERARCHY, 'Combustion technologies with mitigation potential (e.g. using fossil fuels, biomass, waste, etc.)' EN, 'ENE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ENE_4_1' ENV_TECH, 'ENE_4' HIERARCHY, 'Technologies for improved output efficiency (combined heat and power, combined cycles, etc.)' EN, 'ENE' Domain_to_publish, 'Industry energy efficiency or substitution' DDS_patent_category, 'Yes' To_keep union
select 'ENE_4_2' ENV_TECH, 'ENE_4' HIERARCHY, 'Technologies for improved input efficiency (efficient combustion or heat usage)' EN, 'ENE' Domain_to_publish, 'Industry energy efficiency or substitution' DDS_patent_category, 'Yes' To_keep union
select 'ENE_5' ENV_TECH, 'ENE' HIERARCHY, 'Technologies for an efficient electrical power generation, transmission or distribution' EN, 'ENE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ENE_5_1' ENV_TECH, 'ENE_5' HIERARCHY, 'Superconducting electric elements or equipment' EN, 'ENE' Domain_to_publish, 'Grid' DDS_patent_category, 'Yes' To_keep union
select 'ENE_5_2' ENV_TECH, 'ENE_5' HIERARCHY, 'Smart grids as climate change mitigation technology in the energy generation sector' EN, 'ENE' Domain_to_publish, 'Grid' DDS_patent_category, 'Yes' To_keep union
select 'ENE_5_3' ENV_TECH, 'ENE_5' HIERARCHY, 'Not elsewhere classified' EN, 'ENE' Domain_to_publish, 'Grid' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6' ENV_TECH, 'ENE' HIERARCHY, 'Enabling technologies (technologies with potential or indirect contribution to GHG emission mitigation)' EN, 'ENE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ENE_6_1' ENV_TECH, 'ENE_6' HIERARCHY, 'Energy storage' EN, 'ENE' Domain_to_publish, 'Storage (not e-mobility)' DDS_patent_category, 'No' To_keep union
select 'ENE_6_1_1' ENV_TECH, 'ENE_6_1' HIERARCHY, 'Batteries' EN, 'ENE' Domain_to_publish, 'Storage (not e-mobility)' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6_1_2' ENV_TECH, 'ENE_6_1' HIERARCHY, 'Capacitors' EN, 'ENE' Domain_to_publish, 'Storage (not e-mobility)' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6_1_3' ENV_TECH, 'ENE_6_1' HIERARCHY, 'Mechanical energy storage, e.g. flywheels or pressurised fluids' EN, 'ENE' Domain_to_publish, 'Storage (not e-mobility)' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6_1_4' ENV_TECH, 'ENE_6_1' HIERARCHY, 'Thermal energy storage' EN, 'ENE' Domain_to_publish, 'Storage (not e-mobility)' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6_2' ENV_TECH, 'ENE_6' HIERARCHY, 'Hydrogen technology' EN, 'ENE' Domain_to_publish, 'Hydrogen and fuel cells' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6_3' ENV_TECH, 'ENE_6' HIERARCHY, 'High-voltage direct current transmission' EN, 'ENE' Domain_to_publish, 'Grid' DDS_patent_category, 'Yes' To_keep union
select 'ENE_6_4' ENV_TECH, 'ENE_6' HIERARCHY, 'Fuel cells' EN, 'ENE' Domain_to_publish, 'Hydrogen and fuel cells' DDS_patent_category, 'Yes' To_keep union
select 'ENE_7' ENV_TECH, 'ENE' HIERARCHY, 'Other energy conversion or management systems reducing GHG emissions' EN, 'ENE' Domain_to_publish, 'Energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'ENV_TECH' ENV_TECH, 'TOTAL' HIERARCHY, 'Environment-related technologies' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GHG' ENV_TECH, 'CCM' HIERARCHY, 'Capture, storage, sequestration or disposal of greenhouse gases' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GHG_1' ENV_TECH, 'GHG' HIERARCHY, 'Capture or disposal of nitrous oxide (N2O)' EN, 'GHG' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GHG_2' ENV_TECH, 'GHG' HIERARCHY, 'Capture or disposal of methane (CH4)' EN, 'GHG' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GHG_3' ENV_TECH, 'GHG' HIERARCHY, 'Capture or disposal of perfluorocarbons (PFC), hydrofluorocarbons (HFC) or sulfur hexafluoride (SF6)' EN, 'GHG' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GHG_4' ENV_TECH, 'GHG' HIERARCHY, 'Capture or disposal of carbon dioxide (CO2)' EN, 'GHG' Domain_to_publish, 'Carbon capture and storage' DDS_patent_category, 'Yes' To_keep union
select 'GOODS' ENV_TECH, 'CCM' HIERARCHY, 'Climate change mitigation technologies in the production or processing of goods' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GOODS_1' ENV_TECH, 'GOODS' HIERARCHY, 'Technologies related to metal processing' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GOODS_1_1' ENV_TECH, 'GOODS_1' HIERARCHY, 'Reduction of greenhouse gas (GHG) emissions' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_1_2' ENV_TECH, 'GOODS_1' HIERARCHY, 'Process efficiency' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2' ENV_TECH, 'GOODS' HIERARCHY, 'Technologies relating to chemical industry' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GOODS_2_1' ENV_TECH, 'GOODS_2' HIERARCHY, 'Process efficiency in chemical industry' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2_2' ENV_TECH, 'GOODS_2' HIERARCHY, 'Feedstock' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2_3' ENV_TECH, 'GOODS_2' HIERARCHY, 'Reduction of greenhouse gas [GHG] emissions, e.g. CO2' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2_4' ENV_TECH, 'GOODS_2' HIERARCHY, 'Improvements relating to chlorine production' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2_5' ENV_TECH, 'GOODS_2' HIERARCHY, 'Improvements relating to adipic acid or caprolactam production' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2_6' ENV_TECH, 'GOODS_2' HIERARCHY, 'Improvements relating to fluorochloro hydrocarbon, e.g. chlorodifluoromethane [HCFC-22] production' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_2_7' ENV_TECH, 'GOODS_2' HIERARCHY, 'Improvements relating to the production of bulk chemicals' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_3' ENV_TECH, 'GOODS' HIERARCHY, 'Technologies relating to oil refining and petrochemical industry' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GOODS_3_1' ENV_TECH, 'GOODS_3' HIERARCHY, 'Bio-feedstock' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_3_2' ENV_TECH, 'GOODS_3' HIERARCHY, 'Ethylene production' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_4' ENV_TECH, 'GOODS' HIERARCHY, 'Technologies relating to the processing of minerals' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GOODS_4_1' ENV_TECH, 'GOODS_4' HIERARCHY, 'Production of cement' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_4_2' ENV_TECH, 'GOODS_4' HIERARCHY, 'Production or processing of lime' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_4_3' ENV_TECH, 'GOODS_4' HIERARCHY, 'Glass production' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_4_4' ENV_TECH, 'GOODS_4' HIERARCHY, 'Production of ceramic materials or ceramic elements' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5' ENV_TECH, 'GOODS' HIERARCHY, 'Technologies relating to agriculture, livestock or agroalimentary industries' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'GOODS_5_1' ENV_TECH, 'GOODS_5' HIERARCHY, 'Using renewable energies, e.g. solar water pumping' EN, 'GOODS' Domain_to_publish, 'Agriculture energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_2' ENV_TECH, 'GOODS_5' HIERARCHY, 'Measures for saving energy, e.g. in green houses' EN, 'GOODS' Domain_to_publish, 'Agriculture energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_3' ENV_TECH, 'GOODS_5' HIERARCHY, 'Reduction of greenhouse gas [GHG] emissions in agriculture' EN, 'GOODS' Domain_to_publish, 'Agriculture energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_4' ENV_TECH, 'GOODS_5' HIERARCHY, 'Land use policy measures' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_5' ENV_TECH, 'GOODS_5' HIERARCHY, 'Afforestation or reforestation' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_6' ENV_TECH, 'GOODS_5' HIERARCHY, 'Livestock or poultry management' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_7' ENV_TECH, 'GOODS_5' HIERARCHY, 'Fishing; Aquaculture; Aquafarming' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_5_8' ENV_TECH, 'GOODS_5' HIERARCHY, 'Food processing, e.g. use of renewable energies or variable speed drives in handling, conveying or stacking' EN, 'GOODS' Domain_to_publish, 'Agriculture energy efficiency' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_6' ENV_TECH, 'GOODS' HIERARCHY, 'Technologies in the production process for final industrial or consumer products' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_7' ENV_TECH, 'GOODS' HIERARCHY, 'Climate change mitigation technologies for sector-wide applications' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'GOODS_8' ENV_TECH, 'GOODS' HIERARCHY, 'Enabling technologies with a potential contribution to GHG emissions mitigation' EN, 'GOODS' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'ICT' ENV_TECH, 'CCM' HIERARCHY, 'Climate change mitigation in information and communication technologies (ICT)' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'ICT_1' ENV_TECH, 'ICT' HIERARCHY, 'Energy efficient computing' EN, 'ICT' Domain_to_publish, 'Industry energy efficiency or substitution' DDS_patent_category, 'Yes' To_keep union
select 'ICT_2' ENV_TECH, 'ICT' HIERARCHY, 'Energy efficiency in communication networks' EN, 'ICT' Domain_to_publish, 'Industry energy efficiency or substitution' DDS_patent_category, 'Yes' To_keep union
select 'MAN' ENV_TECH, 'ENV_TECH' HIERARCHY, 'Environmental management' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'MAN_1' ENV_TECH, 'MAN' HIERARCHY, 'Air pollution abatement' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'MAN_1_1' ENV_TECH, 'MAN_1' HIERARCHY, 'Emissions abatement from stationary sources (e.g. SOx, NOx, PM emissions from combustion plants)' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_1_2' ENV_TECH, 'MAN_1' HIERARCHY, 'Emissions abatement from mobile sources (e.g. NOx, CO, HC, PM emissions from motor vehicles)' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_1_3' ENV_TECH, 'MAN_1' HIERARCHY, 'Air pollution abatement - Not elsewhere classified' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_2' ENV_TECH, 'MAN' HIERARCHY, 'Water pollution abatement' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'MAN_2_1' ENV_TECH, 'MAN_2' HIERARCHY, 'Water and wastewater treatment' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_2_2' ENV_TECH, 'MAN_2' HIERARCHY, 'Fertilizers from wastewater' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_2_3' ENV_TECH, 'MAN_2' HIERARCHY, 'Oil spill and pollutant clean-up' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_3' ENV_TECH, 'MAN' HIERARCHY, 'Waste management' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'MAN_3_1' ENV_TECH, 'MAN_3' HIERARCHY, 'Solid waste collection' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_3_2' ENV_TECH, 'MAN_3' HIERARCHY, 'Material recovery, recycling and re-use' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_3_3' ENV_TECH, 'MAN_3' HIERARCHY, 'Fertilizers from waste' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_3_4' ENV_TECH, 'MAN_3' HIERARCHY, 'Incineration and energy recovery' EN, 'MAN' Domain_to_publish, 'Bioenergy' DDS_patent_category, 'Yes' To_keep union
select 'MAN_3_5' ENV_TECH, 'MAN_3' HIERARCHY, 'Waste management – Not elsewhere classified' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_4' ENV_TECH, 'MAN' HIERARCHY, 'Soil remediation' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'MAN_5' ENV_TECH, 'MAN' HIERARCHY, 'Environmental monitoring' EN, 'MAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN' ENV_TECH, 'TOTAL' HIERARCHY, 'Sustainable ocean economy' EN, NULL Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_1' ENV_TECH, 'OCEAN' HIERARCHY, 'Ocean renewable energy generation' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'OCEAN_1_1' ENV_TECH, 'OCEAN_1' HIERARCHY, 'Offshore wind energy' EN, 'OCEAN' Domain_to_publish, 'Wind' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_1_2' ENV_TECH, 'OCEAN_1' HIERARCHY, 'Offshore solar energy' EN, 'OCEAN' Domain_to_publish, 'Solar' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_1_3' ENV_TECH, 'OCEAN_1' HIERARCHY, 'Tide, wave, current and other marine energy' EN, 'OCEAN' Domain_to_publish, 'Other renewables' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_2' ENV_TECH, 'OCEAN' HIERARCHY, 'Ocean pollution abatement' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'OCEAN_2_1' ENV_TECH, 'OCEAN_2' HIERARCHY, 'Ballast water treatment' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_2_2' ENV_TECH, 'OCEAN_2' HIERARCHY, 'Oil spill (and other floating debris) prevention and cleanup' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_3' ENV_TECH, 'OCEAN' HIERARCHY, 'Climate change mitigation in maritime transport' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'OCEAN_3_1' ENV_TECH, 'OCEAN_3' HIERARCHY, 'Improved vessel design' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_3_2' ENV_TECH, 'OCEAN_3' HIERARCHY, 'Fuel-efficient propulsion or fuel substitution' EN, 'OCEAN' Domain_to_publish, 'Vehicle fuel efficiency' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_4' ENV_TECH, 'OCEAN' HIERARCHY, 'Climate change mitigation and adaptation in fishing, aquaculture and aquafarming' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_5' ENV_TECH, 'OCEAN' HIERARCHY, 'Desalination of sea water' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'OCEAN_6' ENV_TECH, 'OCEAN' HIERARCHY, 'Climate change adaptation in coastal zones' EN, 'OCEAN' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'TRA' ENV_TECH, 'CCM' HIERARCHY, 'Climate change mitigation technologies related to transportation' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'TRA_1' ENV_TECH, 'TRA' HIERARCHY, 'Road transport' EN, 'TRA' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'TRA_1_1' ENV_TECH, 'TRA_1' HIERARCHY, 'Conventional vehicles (based on internal combustion engine)' EN, 'TRA' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'TRA_1_2' ENV_TECH, 'TRA_1' HIERARCHY, 'Hybrid vehicles' EN, 'TRA' Domain_to_publish, 'E-mobility' DDS_patent_category, 'Yes' To_keep union
select 'TRA_1_3' ENV_TECH, 'TRA_1' HIERARCHY, 'Electric vehicles' EN, 'TRA' Domain_to_publish, 'E-mobility' DDS_patent_category, 'Yes' To_keep union
select 'TRA_1_4' ENV_TECH, 'TRA_1' HIERARCHY, 'Fuel efficiency-improving vehicle design (common to all road vehicles)' EN, 'TRA' Domain_to_publish, 'Vehicle fuel efficiency' DDS_patent_category, 'Yes' To_keep union
select 'TRA_2' ENV_TECH, 'TRA' HIERARCHY, 'Rail transport' EN, 'TRA' Domain_to_publish, 'Air - Rail - Marine' DDS_patent_category, 'Yes' To_keep union
select 'TRA_3' ENV_TECH, 'TRA' HIERARCHY, 'Maritime or waterways transport' EN, 'TRA' Domain_to_publish, 'Air - Rail - Marine' DDS_patent_category, 'Yes' To_keep union
select 'TRA_4' ENV_TECH, 'TRA' HIERARCHY, 'Aeronautics or air transport' EN, 'TRA' Domain_to_publish, 'Air - Rail - Marine' DDS_patent_category, 'Yes' To_keep union
select 'TRA_5' ENV_TECH, 'TRA' HIERARCHY, 'Enabling technologies in transport' EN, 'TRA' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'TRA_5_1' ENV_TECH, 'TRA_5' HIERARCHY, 'Electric vehicle charging' EN, 'TRA' Domain_to_publish, 'E-mobility' DDS_patent_category, 'Yes' To_keep union
select 'TRA_5_2' ENV_TECH, 'TRA_5' HIERARCHY, 'Application of hydrogen technology to transportation, e.g. using fuel cells' EN, 'TRA' Domain_to_publish, 'Hydrogen and fuel cells' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE' ENV_TECH, 'CCM' HIERARCHY, 'Climate change mitigation technologies related to wastewater treatment or waste management' EN, NULL Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'WAT_WASTE_1' ENV_TECH, 'WAT_WASTE' HIERARCHY, 'Wastewater treatment' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2' ENV_TECH, 'WAT_WASTE' HIERARCHY, 'Solid waste management' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'WAT_WASTE_2_1' ENV_TECH, 'WAT_WASTE_2' HIERARCHY, 'Waste collection, transportation, transfer or storage' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_2' ENV_TECH, 'WAT_WASTE_2' HIERARCHY, 'Waste processing or separation' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_3' ENV_TECH, 'WAT_WASTE_2' HIERARCHY, 'Landfill technologies aiming to mitigate methane emissions' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_4' ENV_TECH, 'WAT_WASTE_2' HIERARCHY, 'Bio-organic fraction processing; Production of fertilisers from the organic fraction of waste or refuse' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5' ENV_TECH, 'WAT_WASTE_2' HIERARCHY, 'Reuse, recycling or recovery technologies' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'No' To_keep union
select 'WAT_WASTE_2_5_1' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Mechanical processing of waste for the recovery of materials, e.g. crushing, shredding, separation or disassembly' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_10' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Packaging reuse or recycling, e.g. of multilayer packaging' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_11' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Recycling of waste of electrical or electronic equipment (WEEE)' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_12' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Recycling of batteries or fuel cells' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_13' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Use of waste materials as fillers for mortars or concrete' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_2' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Waste management of vehicles' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_3' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Construction or demolition [C&D] waste' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_4' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Glass recycling' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_5' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Plastics and rubber recycling' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_6' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Paper recycling' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_7' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Disintegrating fibre-containing textile articles to obtain fibres for re-use' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_8' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Recovery of fats, fatty oils, fatty acids or other fatty substances, e.g. lanolin or waxes' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_2_5_9' ENV_TECH, 'WAT_WASTE_2_5' HIERARCHY, 'Recycling of wood or furniture waste' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep union
select 'WAT_WASTE_3' ENV_TECH, 'WAT_WASTE' HIERARCHY, 'Enabling technologies or technologies with a potential or indirect contribution to GHG emissions mitigation' EN, 'WAT_WASTE' Domain_to_publish, '' DDS_patent_category, 'Yes' To_keep 
)a