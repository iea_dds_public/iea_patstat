/*
Author:			Ashley ACKER (IEA), Laurie CIARAMELLA, Aloys NGHIEM (IEA)
Date:			July 2022
Description:	Creates the FF_tech table, which lists the fossil fuel supply related patents (appln_id), 
				by supply category and subcategory. It follows the procedure described in the document 
				entitled "Methodology for identifying fossil fuel supply related technologies in 
				patent data" (see References folder). 
				
Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: 
					upstream, process_downstream, transmission_distribution, FF_tech
				The parameters of the function used in this script are the following: 
					patent_filter(@cpc, @cpc_with_percent, @cpc_with_range, @text_searches_to_include,  
					@text_searches_to_exclude) 

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
				Similarly, the following needs to be modified to match the schema that was used to replace [Division_EDC].[patstats]: 
					'USE Division_EDC' and 'patstats.'			
				In addition, the schema in the function 'patent_filter' need to be modified (see 'Fossil fuels - CPC\FUNCTION_patent_filter'). 
				The function (patent_filter) needs to be created in order for this script to run.

Methodology:	Please note that Regex syntax and commands are not well supported by SQL server in the context 
				of "LIKE" operator, which is used in the present code for text-filtering queries. A more efficient 
				solution, both in terms of speed and of compliance to the filtering queries from the original documentation, 
				would have been not to use the "LIKE" operator, but to instead use the "CONTAINS" operator (and subsequent 
				operators, e.g. "NEAR"). These operators use full-text search, and hence require full-text indexed columns. 
				After several discussions with Florian Mante from OECD, we could not get the confirmation that the column 
				"appln_abstract" from TLS203 was, or could be full-text indexed  in the Patstat version the ENV division has 
				access to. Hence, it was recommended to write the filtering queries with the LIKE command. Please note that 
				1) it is not optimal speed-wise, 2) some of the specificities of the filtering text queries from the original 
				documentation could not be fully translated into the code (e.g. "w", "2w", "3w", for which the present code is 
				less restrictive).
*/ 

create table [Division_EDC].[patstats].upstream(
	appln_id int ,
	FF_supply_category varchar(300) ,
	FF_supply_subcategory	varchar(300)
	)
create table [Division_EDC].[patstats].process_downstream(
	appln_id int ,
	FF_supply_category varchar(300) ,
	FF_supply_subcategory	varchar(300)
	)
create table [Division_EDC].[patstats].transmission_distribution(
	appln_id int ,
	FF_supply_category varchar(300) ,
	FF_supply_subcategory	varchar(300)
	)
create table [Division_EDC].[patstats].FF_tech(
	appln_id int ,
	FF_supply_category varchar(300) ,
	FF_supply_subcategory	varchar(300)
	) */

USE Division_EDC  -- This needs to be defined at the beginning for the function schema to be complete, since a function can only be preceeded by one table name

------------------------------------------------------------------
------------------------------------------------------------------
--------- 1. Upstream --------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
-- The temp table below saves a list of application IDs that have CPCs we would like to exclude. It is used many times in the below query for 1.1 
select distinct appln_id 
into #t_filter_1_1
from  patstats.patent_filter( '{none}', 'E21B  43/26%' , '{none}', '{none}', '{none}')
					 union select distinct appln_id from  patstats.patent_filter( 'E21B  43/27', '{none}' , '{none}', '{none}', '{none}')
				     union select distinct appln_id from  patstats.patent_filter( '{none}', '{none}', 'E21B   7/04 to E21B   7/10' , '{none}', '{none}')
					 union select distinct appln_id from  patstats.patent_filter( '{none}', '{none}', 'E21B  43/16 to E21B  43/248' , '{none}', '{none}')
					 union select distinct appln_id from  patstats.patent_filter( '{none}', 'E21B  43/24%' , '{none}', '{none}', '{none}')
					 union select distinct appln_id from  patstats.patent_filter( 'E21B  43/006' , '{none}', '{none}', '{none}', '{none}')
					 union select distinct appln_id from  patstats.patent_filter( 'E21B  41/0099' , '{none}', '{none}', '{none}', '{none}')

------------------------------------------------------------------
--- 1.1 Conventional oil and gas exploratation and extraction ----
------------------------------------------------------------------  
drop table if exists #t1_1 

select distinct appln_id
into #t1_1
from patstats.patent_filter( 'B03B   9/02', '{none}' , '{none}' , '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'B03D2203/006', '{none}' , '{none}' , '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'B63B  35/4413', '{none}' , '{none}' , '{1}, {2}, {4}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'B63B2035/442', '{none}' , '{none}' , '{1}, {2}, {4}', '{none}')  
union
select distinct appln_id from  patstats.patent_filter( 'B63B2035/448', '{none}' , '{none}' , '{1}, {2}, {4}', '{none}')  
union
select distinct appln_id from  patstats.patent_filter( 'B63B  75/00', '{none}', '{none}' , '{1}, {2}, {4}', '{none}')  
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'C09K   8/%' ,  '{none}' , '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'C10L   5/04', '{none}' , '{none}', '{none}' , '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}' , 'E02B  17/0%', '{none}' , '{1}, {8}', '{12}')  
union
select distinct appln_id from  patstats.patent_filter( '{none}' , '{none}' , 'E02B2017/0039 to E02B2017/0047', '{1}, {8}', '{12}')
union
select distinct appln_id from  patstats.patent_filter( '{none}' , '{none}' , 'E02B2017/0056 to E02B2017/0095', '{1}, {8}', '{12}')
union
select distinct appln_id from  patstats.patent_filter( '{none}' , 'E02B2201/%' , '{none}', '{1}, {8}', '{12}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'E21B   1/00 to E21B  49/10' , '{1}, {2}, {4}, {8}', '{none}')
	where appln_id not in (select distinct appln_id from #t_filter_1_1)

alter table #t1_1 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t1_1 set FF_supply_category = 'Upstream' , FF_supply_subcategory = 'Conventional oil and gas exploration and extraction'

------------------------------------------------------------------
--- 1.2 Unconventional oil and gas exploration and extraction ----
------------------------------------------------------------------
drop table if exists #t1_2 

select distinct appln_id 
into #t1_2
from  patstats.patent_filter( '{none}', 'E21B  43/26%' , '{none}' , '{1}, {2}, {4}, {8}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'E21B  43/27', '{none}' , '{none}' , '{1}, {2}, {4}, {8}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'E21B   7/04 to E21B   7/10' , '{1}, {2}, {4}, {8}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'E21B  43/16 to E21B  43/248' , '{1}, {2}, {4}, {8}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'E21B  43/006', '{none}' , '{none}' , '{1}, {2}, {4}, {8}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'E21B  41/0099', '{none}' , '{none}' , '{1}, {2}, {4}, {8}', '{none}')

alter table #t1_2 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t1_2 set FF_supply_category = 'Upstream' , FF_supply_subcategory = 'Unconventional oil and gas exploration and extraction'

------------------------------------------------------------------
--- 1.3 Coal and solid fuels exploration and mining --------------
------------------------------------------------------------------
drop table if exists #t1_3 

select distinct appln_id 
into  #t1_3
from patstats.patent_filter( 'B03B   9/005', '{none}' , '{none}' , '{none}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B03B   1/%' , '{none}' , '{1}, {3}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B03D2203/08', '{none}' , '{none}' , '{none}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D  11/%' , '{none}' , '{1}, {3}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'E21C%' , '{none}' , '{1}, {3}', '{none}')

alter table #t1_3 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t1_3 set FF_supply_category = 'Upstream' , FF_supply_subcategory = 'Coal and solid fuels exploration and mining'

------------------------------------------------------------------
-- Appending all "upstream" tables
------------------------------------------------------------------
truncate table [Division_EDC].[patstats].upstream 
insert into [Division_EDC].[patstats].upstream
select * 
from          #t1_1 union 
select * from #t1_2 union 
select * from #t1_3

------------------------------------------------------------------
------------------------------------------------------------------
--------- 2. Processing and downstream ---------------------------
------------------------------------------------------------------
------------------------------------------------------------------

-- filter query to remove C10L2200/0469 used in 2.1
select distinct appln_id 
into #t_filter_C10L2200_0469LOW
from  patstats.patent_filter( '{none}', '{none}' , 'C10L2200/0469 to C10L2200/0492' , '{none}', '{none}') 
 
------------------------------------------------------------------
--- 2.1 Oil refining ---------------------------------------------
------------------------------------------------------------------
drop table if exists #t2_1

select distinct appln_id 
into  #t2_1
from patstats.patent_filter( '{none}', '{none}' , 'C10G   1/00 to C10G   1/042' , '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'C10G   1/047 to C10G  99/00' , '{none}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'C10L   1/%' , '{none}' , '{1}, {2}, {5}', '{10}') 
	where appln_id not in (select distinct appln_id from  #t_filter_C10L2200_0469LOW)
	
alter table #t2_1 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t2_1 set FF_supply_category = 'Processing and downstream' , FF_supply_subcategory = 'Oil refining'

------------------------------------------------------------------
--- 2.2 Gas conditioning ------------------------------------------
------------------------------------------------------------------
drop table if exists #t2_2

select distinct appln_id 
into  #t2_2
from patstats.patent_filter( '{none}', 'C10K%' , '{none}' , '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'C10L   3/06 to C10L   3/108' , '{4}, {6}', '{10}')
	where appln_id not in (select appln_id from  #t_filter_C10L2200_0469LOW)
union 
select distinct appln_id from  patstats.patent_filter( 'F25J   3/0209' , '{none}',  '{none}' , '{none}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'F25J   3/0214' , '{none}',  '{none}' , '{none}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'F25J   3/061' , '{none}', '{none}' , '{none}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'F25J   3/0615' , '{none}', '{none}' , '{none}', '{none}')
 
alter table #t2_2 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t2_2 set FF_supply_category = 'Processing and downstream' , FF_supply_subcategory = 'Gas conditioning'

------------------------------------------------------------------
--- 2.3 Solid Fuel conditioning ----------------------------------
------------------------------------------------------------------
drop table if exists #t2_3

select distinct appln_id 
into  #t2_3
from patstats.patent_filter( '{none}', 'C10F%' , '{none}' , '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' ,'C10L   5/06 to C10L   5/22' ,  '{3} , {7}', '{10}')
	where appln_id not in (select appln_id from  #t_filter_C10L2200_0469LOW)
union 
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'C10L   5/24 to C10L   5/38' , '{3} , {7}', '{10}')
	where appln_id not in (select appln_id from  #t_filter_C10L2200_0469LOW)

alter table #t2_3 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t2_3 set FF_supply_category = 'Processing and downstream' , FF_supply_subcategory = 'Solid fuel conditioning'

------------------------------------------------------------------
--- 2.4 Coal to Gas ----------- ----------------------------------
------------------------------------------------------------------
drop table if exists #t2_4

select distinct appln_id 
into  #t2_4
from patstats.patent_filter( '{none}', 'C10B%' , '{none}' , '{none}', '{none}') 
	where appln_id not in (select appln_id from  patstats.patent_filter( '{none}', '{none}' , 'C10B  53/00 to C10B  53/02' ,  '{none}', '{none}'))
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}' , 'C10J   1/00 to C10J   3/86' , '{none}', '{none}') 

alter table #t2_4 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t2_4 set FF_supply_category = 'Processing and downstream' , FF_supply_subcategory = 'Coal-to-gas'

------------------------------------------------------------------
--- 2.5 Coal to Liquids and gas to liquids -----------------------
------------------------------------------------------------------
-- Note: This category requires patents to have two distinct CPCs 
drop table if exists #t2_5

select distinct first_cpc_table.appln_id 
into  #t2_5
from           (select distinct appln_id  from patstats.patent_filter('{none}' , '{none}' , 'C01B   3/22 to C01B   3/48', '{none}', '{none}')) as first_cpc_table         
	inner join (select distinct appln_id  from patstats.patent_filter( 'C01B2203/062', '{none}' ,  '{none}', '{none}', '{none}')) as second_cpc_table       
	on first_cpc_table.appln_id = second_cpc_table.appln_id 
union
	(select distinct first_cpc_table.appln_id 
	from       (select distinct appln_id  from patstats.patent_filter('{none}' , 'C10J   3/%' , '{none}', '{none}', '{none}')) as first_cpc_table         
	inner join (select distinct appln_id  from patstats.patent_filter( '{none}', 'C10G   2/3%' , '{none}', '{none}', '{none}')) as second_cpc_table 
	on first_cpc_table.appln_id = second_cpc_table.appln_id)

alter table #t2_5 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t2_5 set FF_supply_category = 'Processing and downstream' , FF_supply_subcategory = 'Coal-to-liquids and gas-to-liquids'

------------------------------------------------------------------
--- 2.6 Hydrogen fuel production ---------------------------------
------------------------------------------------------------------
drop table if exists #t2_6

select distinct appln_id 
into  #t2_6
from patstats.patent_filter('{none}' , '{none}' , 'C01B   3/22 to C01B   3/48', '{none}', '{none}')

alter table #t2_6 add FF_supply_category varchar(300) , FF_supply_subcategory varchar(300)
update #t2_6 set FF_supply_category = 'Processing and downstream' , FF_supply_subcategory = 'Hydrogen fuel production'

------------------------------------------------------------------
-- Appending all "processing and downstream" tables
------------------------------------------------------------------
truncate table [Division_EDC].[patstats].process_downstream
insert into [Division_EDC].[patstats].process_downstream
select * 
from          #t2_1 union
select * from #t2_2 union 
select * from #t2_3 union 
select * from #t2_4 union 
select * from #t2_5 union 
select * from #t2_6

------------------------------------------------------------------
------------------------------------------------------------------
--------- 3. Transmission and distribution  ----------------------
------------------------------------------------------------------
------------------------------------------------------------------

------------------------------------------------------------------
--- 3.1 Liquid fuel pipelines ------------------------------------
------------------------------------------------------------------
drop table if exists #t3_1

select distinct appln_id 
into  #t3_1
from patstats.patent_filter( 'B63B  27/34' , '{none}', '{none}', '{2}, {5}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'B63B  27/24' , '{none}', '{none}', '{2}, {5}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B63B  27/25' , '{none}', '{none}', '{2}, {5}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'F17D%' , '{none}', '{2}, {5}', '{none}')

alter table #t3_1 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_1 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Liquid fuel pipelines'

------------------------------------------------------------------
--- 3.2 Gas fuel pipelines ------------------------------------
------------------------------------------------------------------
drop table if exists #t3_2

select distinct appln_id 
into  #t3_2
from patstats.patent_filter( 'B63B  27/24' , '{none}', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B63B  27/25' , '{none}', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'F17D   1/04' , '{none}', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'F17D   1/05' , '{none}', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}', 'F17D   1/065 to F17D   1/075' , '{4}, {6}', '{none}')

alter table #t3_2 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_2 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Gas fuel pipelines'

------------------------------------------------------------------
--- 3.3 Liquid fuel tanker shipping  -----------------------------
------------------------------------------------------------------
drop table if exists #t3_3

select distinct appln_id 
into  #t3_3
from patstats.patent_filter( '{none}', 'B63B  25/08 to B63B  25/16' , '{none}', '{2}, {5}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'B63B2025/085' , '{none}', '{none}', '{2}, {5}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B63B2025/087' , '{none}', '{none}', '{2}, {5}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B67D   9/0%' , '{none}', '{2}, {5}', '{none}')

alter table #t3_3 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_3 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Liquid fuel tanker shipping'

------------------------------------------------------------------
--- 3.4 Liquified gaseous fuel shipping   ------------------------
------------------------------------------------------------------
drop table if exists #t3_4

select distinct appln_id 
into  #t3_4
from patstats.patent_filter( 'F25J   1/0022' , '{none}', '{none}', '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'F25J   1/0025' , '{none}', '{none}', '{none}', '{none}')

alter table #t3_4 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_4 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Liquified gaseous fuel shipping'

------------------------------------------------------------------
--- 3.5 Compressed gaseous fuel shipping  ------------------------
------------------------------------------------------------------
drop table if exists #t3_5

select distinct appln_id 
into  #t3_5
from patstats.patent_filter( 'B63B2025/087' , '{none}', '{none}', '{4}, {6}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( 'B63B  25/14', '{none}', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B63B  25/16', '{none}', '{none}', '{4}, {6}', '{none}')

alter table #t3_5 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_5 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Compressed gaseous fuel shipping' 

------------------------------------------------------------------
--- 3.6 Solid fuel shipping ------------- ------------------------
------------------------------------------------------------------
drop table if exists #t3_6

select distinct appln_id 
into  #t3_6
from patstats.patent_filter( 'B63B  25/04' , '{none}', '{none}', '{3}', '{none}') 

alter table #t3_6 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_6 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Solid fuel shipping' 

------------------------------------------------------------------
--- 3.7 Road tanker liquid fuels transport   ---------------------
------------------------------------------------------------------
drop table if exists #t3_7

select distinct appln_id 
into  #t3_7
from patstats.patent_filter( '{none}', '{none}', 'B60P   3/22 to B60P   3/246' , '{5}', '{none}') 
where (    ('.' + appln_abstract + '.') like '%[^a-z]tank[^a-z]%'
		or ('.' + appln_abstract + '.') like '%[^a-z]reservoir[^a-z]%'
		or ('.' + appln_abstract + '.') like '%[^a-z]citerne[^a-z]%' ) 

alter table #t3_7 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_7 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Road tanker liquid fuels transport'

------------------------------------------------------------------
--- 3.8 Road tanker gaseous fuels transport   ---------------------
------------------------------------------------------------------
drop table if exists #t3_8

select distinct appln_id 
into  #t3_8
from patstats.patent_filter('{none}', '{none}', 'B60P   3/22 to B60P   3/246' , '{6}', '{none}') 
where (    ('.' + appln_abstract + '.') like '%[^a-z]tank[^a-z]%'
		or ('.' + appln_abstract + '.') like '%[^a-z]reservoir[^a-z]%'
		or ('.' + appln_abstract + '.') like '%[^a-z]citerne[^a-z]%' )

alter table #t3_8 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_8 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Road tanker gaseous fuels transport'

------------------------------------------------------------------
--- 3.9 Rail tanker liquid fuels transport   ---------------------
------------------------------------------------------------------
drop table if exists #t3_9

select distinct appln_id 
into  #t3_9
from patstats.patent_filter( '{none}', 'B61D   5/%' , '{none}', '{2}, {5}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D   3/%', '{none}', '{2}, {5}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D  15/%', '{none}', '{2}, {5}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B61D  49/00', '{none}', '{none}', '{2}, {5}', '{none}')

alter table #t3_9 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_9 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Rail tanker liquid fuels transport'

------------------------------------------------------------------
--- 3.10 Rail tanker gaseous fuels transport   ---------------------
------------------------------------------------------------------
drop table if exists #t3_10

select distinct appln_id 
into  #t3_10
from patstats.patent_filter( '{none}', 'B61D   3/%' , '{none}', '{4}, {6}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D  15/%', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B61D  49/00', '{none}', '{none}', '{4}, {6}', '{none}')

alter table #t3_10 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_10 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Rail tanker gaseous fuels transport'

------------------------------------------------------------------
--- 3.11 Rail solid fuels transport   ---------------------
------------------------------------------------------------------
drop table if exists #t3_11

select distinct appln_id 
into  #t3_11
from patstats.patent_filter( '{none}', 'B61D   7/%' , '{none}', '{3}, {7}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D   9/%', '{none}', '{3}, {7}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D   3/%', '{none}', '{3}, {7}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'B61D  15/%', '{none}', '{3}, {7}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( 'B61D  49/00', '{none}', '{none}', '{3}, {7}', '{none}')

alter table #t3_11 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_11 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Rail solid fuels transport'

------------------------------------------------------------------
--- 3.12 Underground liquid fuels storage     ---------------------
------------------------------------------------------------------
drop table if exists #t3_12

select distinct appln_id 
into  #t3_12
from patstats.patent_filter( '{none}', 'B65G   5/00%' , '{none}', '{1}, {2}, {5}', '{none}') 

alter table #t3_12 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_12 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Underground liquid fuels storage'

------------------------------------------------------------------
--- 3.13 Underground gaseous fuel storage     ---------------------
------------------------------------------------------------------
drop table if exists #t3_13

select distinct appln_id 
into  #t3_13
from patstats.patent_filter( '{none}', '{none}', 'F17C2270/0142 to F17C2270/0163' , '{4}, {6}', '{none}') 

alter table #t3_13 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_13 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Underground gaseous fuel storage'

------------------------------------------------------------------
--- 3.14 Stationary tank storage for liquids ---------------------
------------------------------------------------------------------
drop table if exists #t3_14

select distinct appln_id 
into  #t3_14
from patstats.patent_filter( 'E02D  27/38' , '{none}', '{none}', '{2}, {5}', '{none}') 

alter table #t3_14 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_14 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Stationary tank storage for liquids'

------------------------------------------------------------------
--- 3.15 Stationary tank storage for gases ---------------------
------------------------------------------------------------------
drop table if exists #t3_15

select distinct appln_id 
into  #t3_15
from patstats.patent_filter( 'F17B   1/26' , '{none}', '{none}', '{4}, {6}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', 'F17C  %', '{none}', '{4}, {6}', '{none}')
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}', 'F17C2221/032 to F17C2221/036', '{none}', '{none}')

alter table #t3_15 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_15 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Stationary tank storage for gases'

------------------------------------------------------------------
--- 3.16 Solid fuel storage     ---------------------
------------------------------------------------------------------
drop table if exists #t3_16

select distinct appln_id 
into  #t3_16
from patstats.patent_filter( '{none}', 'B65G   3/00%' , '{none}', '{3}, {7}', '{none}') 

alter table #t3_16 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_16 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Solid fuel storage'

------------------------------------------------------------------
--- 3.17 Liquid fuel distribution (gas stations) -----------------
------------------------------------------------------------------
drop table if exists #t3_17

select distinct appln_id 
into  #t3_17
from patstats.patent_filter( 'G01M   3/2892' , '{none}', '{none}', '{none}', '{none}') 
union
select distinct appln_id from  patstats.patent_filter( '{none}', '{none}', 'G01M   3/32 to G01M   3/34', '{5}', '{none}')

alter table #t3_17 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_17 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Liquid fuel distribution (gas stations)'

------------------------------------------------------------------
--- 3.18 Gaseous fuel distribution     ---------------------------
------------------------------------------------------------------
drop table if exists #t3_18

select distinct appln_id 
into  #t3_18
from patstats.patent_filter( '{none}', 'F17C2265/06%' , '{none}', '{4}, {6}', '{none}') 

alter table #t3_18 add FF_supply_category varchar(300), FF_supply_subcategory varchar(300)
update #t3_18 set FF_supply_category = 'Transmission and distribution' , FF_supply_subcategory = 'Gaseous fuel distribution'

------------------------------------------------------------------
-- Appending all "transmission and distribution" tables
------------------------------------------------------------------
truncate table [Division_EDC].[patstats].transmission_distribution
insert into [Division_EDC].[patstats].transmission_distribution 
select * 
from          #t3_1 union
select * from #t3_2 union 
select * from #t3_3 union 
select * from #t3_4 union 
select * from #t3_5 union 
select * from #t3_6 union
select * from #t3_7 union 
select * from #t3_8 union 
select * from #t3_9 union 
select * from #t3_10 union 
select * from #t3_11 union
select * from #t3_12 union 
select * from #t3_13 union 
select * from #t3_14 union 
select * from #t3_15 union 
select * from #t3_16 union
select * from #t3_17 union
select * from #t3_18 

------------------------------------------------------------------
-- Creation of the final table ----------------------------
------------------------------------------------------------------
truncate table [Division_EDC].[patstats].FF_tech
insert into [Division_EDC].[patstats].FF_tech 
select * from [Division_EDC].[patstats].upstream
union
select * from [Division_EDC].[patstats].process_downstream
union
select * from  [Division_EDC].[patstats].transmission_distribution