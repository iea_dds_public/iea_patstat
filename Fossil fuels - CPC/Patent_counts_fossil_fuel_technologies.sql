/*
Author:			Ashley ACKER (IEA)
Date:			July 2022
Description:	Creates a table (FF_fractional_patent_counts) with fractional patent counts for fossil fuel 
				technologies by supply categories and subcategories. All patent counts are by country, year 
				and technology. 

Notes:			Tables should only be created the first time the script is run.
				Tables referenced in script: FF_tech, FF_app_country_year, FF_fractional_patent_counts,
					[SPRING22_APPLN], [SPRING22_PERS_APPLN], [SPRING22_PERSON]

Important:		The user needs to replace the following schema to match the schema of their environment:
					[Division_EDC].[patstats] -- this schema is where all created tables will be saved
					[GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo] -- this schema is where the original PATSTAT data is stored
*/

create table [Division_EDC].[patstats].FF_app_country_year( -- Note: Intermediary table that was not created as a temp since it takes >1h to generate
	appln_id int, 
	FF_supply_category varchar(300),
	FF_supply_subcategory varchar(300),
	CountryISO2 varchar(5),
	Patent_year int ,
	person_id int )
create table [Division_EDC].[patstats].FF_fractional_patent_counts(
	CountryISO2 varchar(5),
	Patent_year int,
	Category varchar(300),
	Fractional_patent_count  decimal(10,2) )

-- The FF_app_country_year is an intermediary table that will be used to create the fractional patent counts. 
-- It adds inventor country and earliest publication year to the table of fossil fuel patents. 
-- Each row corresponds to an inventor of a given patent application.
truncate table [Division_EDC].[patstats].FF_app_country_year 
insert into[Division_EDC].[patstats].FF_app_country_year
select ff.appln_id, ff.FF_supply_category, ff.FF_supply_subcategory, pers.person_ctry_code as CountryISO2, app.earliest_publn_year as Patent_year, pers.person_id  
from [Division_EDC].[patstats].FF_tech ff
left join (SELECT * FROM [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_APPLN]) app on app.appln_id = ff.appln_id
left join (SELECT * FROM [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_PERS_APPLN]) pers_ap on ff.appln_id = pers_ap.appln_id
left join (SELECT * FROM [GEN-SQL14-1.MAIN.OECD.ORG].[PATSTAT].[dbo].[SPRING22_PERSON]) pers on pers.person_id = pers_ap.person_id
-- We only want the resulting table to include patent that were granted and have family size >=2 and people that are inventors and the country is known 
where docdb_family_size>=2 and invt_seq_nr > 0 and person_ctry_code is not NULL and person_ctry_code !='' and earliest_publn_year!='9999'

-------------------------------------------------------------------------------
-- Patent count prep for fossil fuel subcategories
-------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
drop table if exists #table_subcategory
select Patent_year, FF_supply_subcategory as Category, appln_id , CountryISO2,  cast(count(distinct person_id ) as decimal(38,2)) as Country_people
into #table_subcategory
from [Division_EDC].[patstats].FF_app_country_year
group by appln_id,  Patent_year, FF_supply_subcategory , CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when FF_fractional_patent_counts is filled
drop table if exists #fractional_count_prep_subcategory 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	CAST ( tab.Country_people / tab_country_count.Total_country_people as decimal(38,2)) as Fractional_count_prep
into #fractional_count_prep_subcategory
from 
	(select * from #table_subcategory) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , cast(sum(Country_people) as decimal(38,2)) as Total_country_people
	from #table_subcategory
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------
-- Patent count prep for fossil fuel categories
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
drop table if exists #table_category
select Patent_year, FF_supply_category as Category, appln_id , CountryISO2,  cast(count(distinct person_id ) as decimal(38,2)) as Country_people
into #table_category
from [Division_EDC].[patstats].FF_app_country_year
group by appln_id,  Patent_year, FF_supply_category , CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when FF_fractional_patent_counts is filled
drop table if exists #fractional_count_prep_category 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	CAST ( tab.Country_people / tab_country_count.Total_country_people as decimal(38,2))  as Fractional_count_prep
into #fractional_count_prep_category
from  
	(select * from #table_category) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , cast(sum(Country_people) as decimal(38,2)) as Total_country_people
	from #table_category
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

--------------------------------------------------------------------------------
-- Patent count prep for total of fossil fuels
--------------------------------------------------------------------------------
-- Table that counts the number of people per country (per application ID, year, and technology)
drop table if exists #table_total
select Patent_year, 'Total - fossil fuel technologies' as Category, appln_id , CountryISO2,  cast(count( distinct person_id ) as decimal(38,2)) as Country_people
into #table_total
from [Division_EDC].[patstats].FF_app_country_year
group by appln_id,  Patent_year,  CountryISO2

-- Creating table with fractional count per application ID, country, year and technology, which will be aggregated to get 
-- the fractional counts by country, year and technology when FF_fractional_patent_counts is filled
drop table if exists #fractional_count_prep_total 
select tab.CountryISO2 , tab.Patent_year , tab.Category, tab.appln_id,    
	CAST ( tab.Country_people / tab_country_count.Total_country_people as decimal(38,2))  as Fractional_count_prep
into #fractional_count_prep_total
from 
	(select * from #table_total) tab
	-- Table that counts the number of people for all countries (per application ID, year, and technology). 
left join 
	(select Patent_year, Category, appln_id , cast(sum(Country_people) as decimal(38,2)) as Total_country_people
	from #table_total
	group by appln_id,  Patent_year, Category) tab_country_count
on tab.appln_id = tab_country_count.appln_id and tab.Patent_year = tab_country_count.Patent_year and tab.Category = tab_country_count.Category

----------------------------------------------------------------------------------------------------------
-- Generating final fractional patent counts by country, year and technology for fossil fuels technologies
----------------------------------------------------------------------------------------------------------
truncate table [Division_EDC].[patstats].FF_fractional_patent_counts 
insert into[Division_EDC].[patstats].FF_fractional_patent_counts 
select CountryISO2, Patent_year, Category , sum(Fractional_count_prep) as Fractional_patent_count
from ( select * from  #fractional_count_prep_subcategory
union select * from  #fractional_count_prep_category
union select * from  #fractional_count_prep_total) tab 
group by CountryISO2, Patent_year, Category 
